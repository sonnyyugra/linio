<?php
use PHPUnit\Framework\TestCase;
require_once ("Bot.php");

class LinioTest extends TestCase
{

    public function testPushAndPop()
    {
        $bot = new Bot();
        $this->assertSame('Linio', $bot->printL(3));
        $this->assertSame('linianos', $bot->printL(15));
        $this->assertSame('IT', $bot->printL(20));
        $this->assertSame(11, $bot->printL(11));
        $this->assertSame('linianos', $bot->printL(90));
        $this->assertSame('IT', $bot->printL(100));
    }
}